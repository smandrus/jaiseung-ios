//
//  Favorite.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-30.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import Foundation
import Firebase

class  Favorite
{
    static let current = Favorite()
    private var list = [Restaurant]()
    var count: Int
    {
      return list.count
    }
    
    func item(at row: Int) -> Restaurant
    {
       return list[row]
    }
    
    func append(_ restaurant: Restaurant)
    {
        if list.filter({$0.phoneNumber == restaurant.phoneNumber}).isEmpty
        {
           list.append(restaurant)
           if let user = Auth.auth().currentUser, let phoneNumber = restaurant.phoneNumber, let name = restaurant.name, let urlString = restaurant.url?.absoluteString
           {
              let ref = Database.database().reference(withPath: user.uid).child("favorite")
              ref.child(phoneNumber).setValue(["name": name, "url": urlString])
           }
        }
    }
    
    func remove(at row: Int)
    {
        if let user = Auth.auth().currentUser, let phoneNumber = list[row].phoneNumber
        {
            let ref = Database.database().reference(withPath: user.uid).child("favorite")
            ref.child(phoneNumber).removeValue()
        }
        list.remove(at: row)
    }
    
    func reset()
    {
       list.removeAll()
    }
    
    func load()
    {
        guard let user = Auth.auth().currentUser else {return}
        
        let ref = Database.database().reference(withPath: user.uid).child("favorite")
        ref.observeSingleEvent(of: .value, with:
        {(snapshot) in
          guard let value = snapshot.value as? [String: [String: String]] else {return}
          self.list.removeAll()
          for (phoneNumber, dict) in value
          {
            guard let name = dict["name"], let str = dict["url"], let url = URL(string: str) else {continue}
            let restaurant = Restaurant(name: name, phoneNumber: phoneNumber, url: url)
            self.list.append(restaurant)
          }
        })
    }
}
