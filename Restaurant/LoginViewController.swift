//
//  SignInViewController.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-26.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import UIKit
import Firebase
import FacebookCore
import FacebookLogin

class LoginViewController: UIViewController, LoginButtonDelegate {
    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Log In"
        
        let loginButton = LoginButton(readPermissions: [.publicProfile])
        loginButton.delegate = self
        loginButton.center = view.center
        view.addSubview(loginButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func didPressSignIn(_ sender: UIButton)
    {
        guard let email = inputEmail.text, let password = inputPassword.text else {return}
        
        Auth.auth().signIn(withEmail: email, password: password)
        {(user, error) in
            
           if user == nil
           {
              let alertController = UIAlertController(title: "Login Error", message: error?.localizedDescription ?? "", preferredStyle: .alert)
              let okAction = UIAlertAction(title: "OK", style: .default)
              alertController.addAction(okAction)
              DispatchQueue.main.async {self.present(alertController, animated: true)}
           }
           else
           {
              DispatchQueue.main.async{self.navigationController?.dismiss(animated: true)}
              Favorite.current.load()
           }
        }
    }
    
    @IBAction func didPressSignUp(_ sender: UIButton)
    {
        if let signUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        {
           DispatchQueue.main.async {self.navigationController?.pushViewController(signUpViewController, animated: true)}
        }
    }
    
    @IBAction func didCancel(_ sender: UIBarButtonItem)
    {
       navigationController?.dismiss(animated: true)
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
       switch result
       {
         case .failed(let error):
            print(error)
        
         case .cancelled:
            print("User cancelled login.")
        
         case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
            Auth.auth().signIn(with: credential)
            { (user, error) in
               
               if error == nil
               {
                  DispatchQueue.main.async{self.navigationController?.dismiss(animated: false)}
                  Favorite.current.load()
               }
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton)
    {
       
    }
}
