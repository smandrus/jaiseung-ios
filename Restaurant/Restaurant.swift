//
//  Definition.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-28.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import Foundation

class Restaurant
{
    let name: String?
    let phoneNumber: String?
    let url: URL?
    
    //static var favorite = [Restaurant]()
    
    init(name: String?, phoneNumber: String?, url: URL?)
    {
       self.name = name
       self.phoneNumber = phoneNumber
       self.url = url
    }
}
