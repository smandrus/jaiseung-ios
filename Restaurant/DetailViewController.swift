//
//  DetailViewController.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-28.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import UIKit
import WebKit
import Firebase

class DetailViewController: UIViewController, WKNavigationDelegate {

    var favorite = false
    var restaurant: Restaurant?
    var webView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let webView = webView
        {
          view.addSubview(webView)
        }
        navigationItem.title = restaurant?.name
        if !favorite
        {
          navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "favorite"), style: .plain, target: self, action: #selector(bookmark))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func bookmark()
    {
        guard let restaurant = restaurant else {return}
        
        if Auth.auth().currentUser == nil
        {
            if let loginNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationController") as? UINavigationController
            {
                self.present(loginNavigationController, animated: true)
            }
        }
        else
        {
           Favorite.current.append(restaurant)
        }
    }

}
