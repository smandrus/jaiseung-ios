//
//  RestaurantTableViewController.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-28.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import UIKit
import MapKit
import WebKit
import Firebase
import FacebookCore
import FacebookLogin
import CoreLocation

class RestaurantTableViewController: UITableViewController, CLLocationManagerDelegate {

    @IBOutlet weak var loginButton: UIBarButtonItem!
    
    private let locationManager = CLLocationManager()
    private var restaurants = [Restaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 10.0
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func didPressLogInOut(_ sender: UIBarButtonItem) {
        
        if Auth.auth().currentUser == nil
        {
          if let loginNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationController") as? UINavigationController
          {
             self.present(loginNavigationController, animated: true)
          }
        }
        else
        {
           do
           {
              try Auth.auth().signOut()
              loginButton.title = "Log In"
              Favorite.current.reset()
           }
           catch let signOutError
           {
              print (signOutError.localizedDescription)
           }
            
           let loginManager = LoginManager()
           loginManager.logOut()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth.auth().currentUser == nil
        {
           loginButton.title = "Log In"
        }
        else
        {
           loginButton.title = "Sign Out"
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return restaurants.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantTableViewCell", for: indexPath)

        // Configure the cell...

        cell.textLabel?.text = restaurants[indexPath.row].name
        cell.detailTextLabel?.text = restaurants[indexPath.row].phoneNumber
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "RestaurantToDetail", let row = tableView.indexPathForSelectedRow?.row
        {
           let detailViewController = segue.destination as? DetailViewController
           detailViewController?.webView = WKWebView(frame: view.bounds)
           if let url = restaurants[row].url
           {
              let request = URLRequest(url: url)
              detailViewController?.webView?.load(request)
           }
           detailViewController?.restaurant = restaurants[row]
           detailViewController?.favorite = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.first
        {
           let request = MKLocalSearchRequest()
           request.naturalLanguageQuery = "Restaurant"
           request.region = MKCoordinateRegionMakeWithDistance(location.coordinate, 40000, 40000)
           let search = MKLocalSearch(request: request)
            search.start()
            {(response, error) in
              
                guard let mapItems = response?.mapItems else {return}
                self.restaurants.removeAll()
                for mapItem in mapItems
                {
                    let restaurant = Restaurant(name: mapItem.name, phoneNumber: mapItem.phoneNumber, url: mapItem.url)
                    self.restaurants.append(restaurant)
                }
                DispatchQueue.main.async {self.tableView.reloadData()}
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
