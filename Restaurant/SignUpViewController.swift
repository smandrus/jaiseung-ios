//
//  SignUpViewController.swift
//  Restaurant
//
//  Created by Jaiseung on 2017-05-27.
//  Copyright © 2017 Wavelet Lab. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    @IBOutlet weak var inputConfirmPassword: UITextField!
    
    let imagePicker = UIImagePickerController()
    var inputs = [UITextField]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        inputEmail.text = "mattruch@gmail.com"
        inputPassword.text = "123456"
        inputConfirmPassword.text = "123456"

        navigationItem.title = "Sign Up"
        // Do any additional setup after loading the view.
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [String(kUTTypeImage)]
        imagePicker.delegate = self
        
        inputs = [inputEmail, inputPassword, inputConfirmPassword]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for input in inputs
        {
           input.resignFirstResponder()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func didPressPlusButton(_ sender: UIButton) {
        self.present(imagePicker, animated: true)
    }

    @IBAction func didPressSignUpButton(_ sender: UIButton) {
        
        guard let email = inputEmail.text, let password = inputPassword.text, let confirmPassword = inputConfirmPassword.text else {return}
        
        if password == confirmPassword
        {
           Auth.auth().createUser(withEmail: email, password: password)
           {(user, error) in
             if user == nil
             {
                let alertController = UIAlertController(title: "Login Error", message: error?.localizedDescription ?? "", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default)
                alertController.addAction(okAction)
                DispatchQueue.main.async {self.present(alertController, animated: true)}
             }
             else
             {
                DispatchQueue.main.async{self.navigationController?.dismiss(animated: true)}
                Favorite.current.reset()
             }
           }
        }
        else
        {
           let alertController = UIAlertController(title: "Sign Up Error", message: "Password and confirmation do not match.", preferredStyle: .alert)
           let okAction = UIAlertAction(title: "OK", style: .default)
           alertController.addAction(okAction)
           DispatchQueue.main.async {self.present(alertController, animated: true)}
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as? String
        if mediaType == String(kUTTypeImage), let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
           imageView.image = image
        }
        self.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true)
    }
}
